# vuejs-mobileapp-exemple

Cet exemple vient de mon article sur dev.to
https://dev.to/simerca

Hi **beginners** *or not* !
Today i will show you how to make an awesome mobile app from scratch with VueJS and Capacitor, directly compatible with iOS and Android mobile device.
    
The first question is 
# How it’s work ?

## Did you know CapacitorJS ?
CapacitorJS is full package to set node app compatible with iOS and Android,

The first step is the requirement:

- Android Studio with the latest SDK
- XCode latest version

**and now you can start**.

### Create a basic vue app with @vue/cli packages
```
vue create app
```
set the configuration as you want, not very important at this step.

ok then, 
### install CapacitorJS on your project

```
npm install @capacitor/core @capacitor/cli
``` 

now you can type this command: 
```
npx cap init
```
*respond to the questions,*

**so now very important step,**
inside the capacitor.config.json file, change 
```
webDir: "www"
```

to

```
webDir: "dist"
```

ok you can **build your project** 
```
npm run build
```

and now, it's ready to do **the best thing** :

### Android
```
npx cap add android
```

### iOS
```
sudo gem install cocoapods
npx cap add ios
```

and now do the **magic thing**: 

```
npx cap copy
```
wow !

Now you can open XCode or Android Studio with this command :

```
// Android Studio
npx cap open android
// XCode
npx cap open ios
```

Android Studio 
![Build Android App](https://dev-to-uploads.s3.amazonaws.com/i/wyd0pzqhxzxbjren7zwx.png)

XCode
![Build iOS App](https://dev-to-uploads.s3.amazonaws.com/i/2s487nddp8xyho428eb5.png)

Very awesome no ?

#### Note
For iOs you need to install xcode dependency, follow XCode install instruction to perform this.

and for lazy people
https://gitlab.com/Simerca/vuejs-mobileapp-exemple


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
